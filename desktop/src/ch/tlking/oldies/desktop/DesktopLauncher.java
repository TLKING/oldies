package ch.tlking.oldies.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import ch.tlking.oldies.Oldies;

public class DesktopLauncher {
  public static void main(String[] arg) {
    LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
    config.width = Oldies.width;
    config.height = Oldies.height;
    config.resizable = false;
    config.title = "Good old Oldies";

    new LwjglApplication(new Oldies(), config);
  }
}
