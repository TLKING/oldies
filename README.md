# Oldies #

### What is this repository for? ###

* This is a little exercise to recreate all the good old oldies.
* Version 0.2.2

### How do I get set up? ###

* Clone the repo
* [Set up your libGDX enviorment](https://github.com/libgdx/libgdx/wiki/Setting-up-your-Development-Environment-%28Eclipse%2C-Intellij-IDEA%2C-NetBeans%29) Note: that this project only utilizes the Desktop and Core setups.

### Usefull Links ###

* [libGDX Home](https://libgdx.badlogicgames.com/index.html)
* [libGDX GitHub](https://github.com/libgdx/libgdx/)
* [libGDX Forum](http://badlogicgames.com/forum/)
* [libGDX Blog](http://www.badlogicgames.com/wordpress/)
* [libGDX JavaDoc](https://libgdx.badlogicgames.com/nightlies/docs/api/)

### Rules to help ###
* Keep your word and do your task or say you can not do it.
* Have fun and no stress.
* Create cool content.