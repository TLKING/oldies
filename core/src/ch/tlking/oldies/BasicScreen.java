package ch.tlking.oldies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Gives a Screen the basics that need be
 */
public abstract class BasicScreen implements Screen {
  public final Oldies       game;

  public OrthographicCamera camera;
  public Stage              stage;

  public BasicScreen(final Oldies game) {
    this.game = game;

    this.camera = new OrthographicCamera();
    this.camera.setToOrtho(false, Oldies.width, Oldies.height);

    this.stage = new Stage(new ScreenViewport());

    // add the exit button to every screen on the top left corner
    Button exit;
    exit = new Button(game.skin);
    exit.add("Exit");
    exit.setHeight(20);
    exit.setWidth(50);
    exit.setX(10);
    exit.setY(Oldies.height - 40);
    exit.addListener(new InputListener() {
      public boolean touchDown(InputEvent event, float x, float y, int pointer,
          int button) {
        game.setScreen(new MainMenu(game));
        dispose();
        return false;
      }
    });

    stage.addActor(exit);

    Gdx.input.setInputProcessor(this.stage);
  }

  @Override
  public void show() {
  }

  @Override
  /**
   * resets the background to default and initializes the basics for each screen
   */
  public void render(float delta) {
    Gdx.gl.glClearColor(0, 0, 0, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

    // Restore the stage's viewport.
    stage.getViewport().update(Gdx.graphics.getWidth(),
        Gdx.graphics.getHeight(), true);

    camera.update();
    stage.getBatch().setProjectionMatrix(camera.combined);

    stage.draw();
    stage.act(delta);
  }

  @Override
  public void resize(int width, int height) {
  }

  @Override
  public void pause() {
  }

  @Override
  public void resume() {
  }

  @Override
  public void hide() {
  }

  @Override
  public void dispose() {
    stage.dispose();
  }

}
