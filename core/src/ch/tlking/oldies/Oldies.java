package ch.tlking.oldies;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

/**
 * Is the heart of the Game and will handle all the game wide variables.
 */
public class Oldies extends Game {
  public Skin             skin;
  public static final int width  = 800;
  public static final int height = 480;

  @Override
  public void create() {
    this.createBasicSkin();

    this.setScreen(new MainMenu(this));
  }

  @Override
  public void render() {
    // if not used it will not draw anything!
    super.render();
  }

  @Override
  public void dispose() {
    skin.dispose();
  }

  /**
   * Creates a basic skin to
   */
  private void createBasicSkin() {
    // Create a font
    BitmapFont font = new BitmapFont();
    skin = new Skin();
    skin.add("default", font);

    // Create a texture
    Pixmap pixmap = new Pixmap((int) Gdx.graphics.getWidth() / 4,
        (int) Gdx.graphics.getHeight() / 10, Pixmap.Format.RGB888);
    pixmap.setColor(Color.WHITE);
    pixmap.fill();
    skin.add("background", new Texture(pixmap));

    // Create Button
    ButtonStyle buttonStyle = new ButtonStyle();
    buttonStyle.up = skin.newDrawable("background", Color.GRAY);
    buttonStyle.down = skin.newDrawable("background", Color.DARK_GRAY);
    buttonStyle.checked = skin.newDrawable("background", Color.DARK_GRAY);
    buttonStyle.over = skin.newDrawable("background", Color.LIGHT_GRAY);

    skin.add("default", buttonStyle);

    // Create Style
    LabelStyle labelStyle = new LabelStyle();
    labelStyle.font = font;
    labelStyle.fontColor = Color.WHITE;

    skin.add("default", labelStyle);
  }
}
