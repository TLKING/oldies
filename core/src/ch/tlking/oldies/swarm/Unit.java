package ch.tlking.oldies.swarm;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Circle;

public class Unit {
  protected ShapeRenderer shape;
  protected ShapeType     shapeType;
  protected Circle        hitBox;

  public Unit() {
    this.shape = new ShapeRenderer();
    this.shape.setColor(Color.WHITE);
    this.shapeType = ShapeType.Filled;

    this.hitBox = new Circle();
    this.hitBox.radius = 2;
  }
  
  /**
   * draws the object on the screen
   */
  public void draw() {
    shape.begin(shapeType);
    shape.circle(hitBox.x, hitBox.y, hitBox.radius);
    shape.end();
  }
  
  /**
   * Called when this screen should release all resources.
   */
  public void dispose() {
    shape.dispose();
  }

  /**
   * @return s the hitBox
   */
  public Circle getHitBox() {
    return this.hitBox;
  }
}
