package ch.tlking.oldies.pong.gameObjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;

/**
 * Sets the minimum of statistics for every object
 */
public abstract class GameObject {
  protected ShapeRenderer shape;
  protected ShapeType     shapeType;
  protected Rectangle     hitBox;
  protected float         speed;

  public GameObject(float speed, float width, float height, Color colour,
      ShapeType shapeType) {
    this.speed = speed;

    this.shape = new ShapeRenderer();
    this.shape.setColor(colour);
    this.shapeType = shapeType;

    this.hitBox = new Rectangle();
    this.hitBox.height = height;
    this.hitBox.width = width;
  }

  /**
   * draws the object on the screen
   */
  public void draw() {
    shape.begin(shapeType);
    shape.rect(hitBox.x, hitBox.y, hitBox.width, hitBox.height);
    shape.end();
  }

  /**
   * Called when this screen should release all resources.
   */
  public void dispose() {
    shape.dispose();
  }

  /**
   * @return s the hitBox
   */
  public Rectangle getHitBox() {
    return this.hitBox;
  }
}
