package ch.tlking.oldies.pong.gameObjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import ch.tlking.oldies.Oldies;

/**
 * handels a padel of the Pong game
 */
public class Padle extends GameObject {
  public enum Position {
    LEFT, RIGHT
  }

  public Padle(float speed, float width, float height, Color colour,
      float offset, ShapeType shapeType, Position pos) {
    super(speed, width, height, colour, shapeType);

    if (pos == Position.LEFT) {
      hitBox.x = offset;
    } else {
      hitBox.x = Oldies.width - offset - 10;
    }
    hitBox.y = (Oldies.height - height) / 2;
  }

  /**
   * moves the padel up or down depending on the delta, which is positiv or
   * negativ
   * 
   * @param delta
   */
  public void addToY(float delta) {
    hitBox.y += delta * speed;
    isInbound();
  }

  /**
   * @return s the speed of the padel
   */
  public float getSpeed() {
    return this.speed;
  }

  /**
   * checks whether the padel is inside the screen or not.
   */
  private void isInbound() {
    if (hitBox.y > Oldies.height - hitBox.height) {
      hitBox.y = Oldies.height - hitBox.height;
    }

    if (hitBox.y < 0) {
      hitBox.y = 0;
    }
  }
}
