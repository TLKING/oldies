package ch.tlking.oldies.pong.gameObjects;

import java.util.concurrent.ThreadLocalRandom;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

import ch.tlking.oldies.Oldies;

/**
 * The ball of Pong and all its stats
 */
public class Ball extends GameObject {
  private Vector2 velocity;

  public Ball(float speed, float width, float height, Color colour,
      ShapeType shapeType) {
    super(speed, width, height, colour, shapeType);

    this.reset();

    this.velocity = new Vector2();
    this.resetVelocity(1);
  }

  /**
   * Moves the ball acording to the set velocity
   * 
   * @param delta
   */
  public void move(float delta) {
    // times delta to make sure the speed is consistent throughout the number of
    // frames given
    hitBox.x += velocity.x * delta;
    hitBox.y += velocity.y * delta;
  }

  /**
   * Resets the ball to the center of the screen
   */
  public void reset() {
    hitBox.x = (Oldies.width - hitBox.width) / 2;
    hitBox.y = (Oldies.height - hitBox.height) / 2;
  }

  /**
   * Resets the velocity of the ball. The angel is random
   * 
   * @param direction
   *          1 to apply force towards right. -1 to apply force towards left.
   */
  public void resetVelocity(int direction) {
    // calculates the maximum of Y
    double maxY = Math
        .sqrt(Math.pow(speed, 2)
            / (Math.pow(Oldies.height / 2, 2) + Math.pow(Oldies.width / 2, 2)))
        * (Oldies.height / 2);

    // the minimum is -maxY the max is maxY
    velocity.y = (float) ThreadLocalRandom.current().nextDouble(maxY * -1,
        maxY);

    // calculate X according to Y and the given speed
    velocity.x = (float) Math
        .sqrt(Math.pow(speed, 2) - Math.pow(velocity.y, 2));
  }

  /**
   * set a new velocity
   * 
   * @param x
   * @param y
   */
  public void setVelocity(float x, float y) {
    velocity.x *= x;
    velocity.y *= y;
  }

  /**
   * switch x and y, entry angel = exit angel
   */
  public void switchVelocity() {
    float temp = velocity.x * -1;
    velocity.x = velocity.y;
    velocity.y = temp;
  }
}
