package ch.tlking.oldies.pong;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import ch.tlking.oldies.BasicScreen;
import ch.tlking.oldies.Oldies;
import ch.tlking.oldies.pong.gameObjects.Ball;
import ch.tlking.oldies.pong.gameObjects.Padle;
import ch.tlking.oldies.pong.gameObjects.Padle.Position;

/**
 * Draws the Pong game.
 */
public class Pong extends BasicScreen {
  private GlyphLayout layout;
  private int         scorePlayer1;
  private int         scorePlayer2;
  private float       scoreX;
  private float       scoreY;

  private Padle       player1;
  private Padle       player2;
  private Ball        ball;

  // Stats
  private final int   playerWidth  = 10;
  private final int   playerHeight = 80;
  private final int   playerSpeed  = 350;
  private final float ballWidth    = 10;
  private final float ballHeight   = 10;
  private final float ballSpeed    = 300;

  /**
   * @param game
   */
  public Pong(final Oldies game) {
    super(game);

    // Initialize the score label
    this.scorePlayer1 = 0;
    this.scorePlayer2 = 0;
    this.layout = new GlyphLayout();
    this.updateScore();

    this.ball = new Ball(ballSpeed, ballWidth, ballHeight, Color.WHITE,
        ShapeType.Filled);

    // Initialize player 1
    this.player1 = new Padle(playerSpeed, playerWidth, playerHeight,
        Color.WHITE, 20, ShapeType.Filled, Position.LEFT);

    // Initialize player 2
    this.player2 = new Padle(playerSpeed, playerWidth, playerHeight,
        Color.WHITE, 20, ShapeType.Filled, Position.RIGHT);
  }

  @Override
  public void show() {
  }

  @Override
  public void render(float delta) {
    super.render(delta);

    updateScore();

    // draw the score
    stage.getBatch().begin();
    game.skin.getFont("default").draw(stage.getBatch(), layout, scoreX, scoreY);
    stage.getBatch().end();

    // draw the ball
    ball.draw();

    // draw player 1
    player1.draw();

    // draw player 2
    player2.draw();

    // move Ball
    ball.move(delta);

    // Controls
    if (Gdx.input.isKeyPressed(Keys.UP)) {
      player2.addToY(delta);
    }

    if (Gdx.input.isKeyPressed(Keys.DOWN)) {
      player2.addToY(delta * -1);
    }

    if (Gdx.input.isKeyPressed(Keys.W)) {
      player1.addToY(delta);
    }

    if (Gdx.input.isKeyPressed(Keys.S)) {
      player1.addToY(delta * -1);
    }

    // checks if the ball is out of the screen
    if (ball.getHitBox().x < 0 || ball.getHitBox().x > Oldies.width) {
      if (ball.getHitBox().x < 0) {
        scorePlayer2++;
        ball.resetVelocity(-1);
      }

      if (ball.getHitBox().x > Oldies.width) {
        scorePlayer1++;
        ball.resetVelocity(1);
      }
      ball.reset();
    }

    // checks collision with player
    if (ball.getHitBox().overlaps(player1.getHitBox())
        || ball.getHitBox().overlaps(player2.getHitBox())) {
      if (Gdx.input.isKeyPressed(Keys.UP) || Gdx.input.isKeyPressed(Keys.DOWN)
          || Gdx.input.isKeyPressed(Keys.W) || Gdx.input.isKeyPressed(Keys.S)) {
        ball.setVelocity(-1.1f, 1.1f);
      } else {
        ball.setVelocity(-1, 1);
      }
    }

    // checks collision with balls
    if (ball.getHitBox().y > (Oldies.height - ballHeight)
        || ball.getHitBox().y < 0) {
      ball.setVelocity(1, -1);
    }
  }

  @Override
  public void dispose() {
    ball.dispose();
    player1.dispose();
    player2.dispose();
  }

  /**
   * Updates the score on the label, according to the data
   */
  private void updateScore() {
    layout.setText(game.skin.getFont("default"),
        scorePlayer1 + ":" + scorePlayer2);
    scoreX = (Oldies.width - layout.width) / 2;
    scoreY = Oldies.height - 30;
  }
}
