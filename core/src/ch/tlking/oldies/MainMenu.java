package ch.tlking.oldies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import ch.tlking.oldies.pong.Pong;
import ch.tlking.oldies.snake.Snake;
import ch.tlking.oldies.spaceInvader.SpaceInvader;
import ch.tlking.oldies.swarm.Swarm;
import ch.tlking.oldies.vr4m.vr4m;

/**
 * Draws the main menu.
 */
public class MainMenu extends BasicScreen {
  private GlyphLayout layout;
  private float       menuX;
  private float       menuY;

  public MainMenu(final Oldies game) {
    super(game);

    this.layout = new GlyphLayout(game.skin.getFont("default"), "Menu");
    this.menuX = (Oldies.width - this.layout.width) / 2;
    this.menuY = Oldies.height - 30;

    this.createMenu();

    // removes all actors of the stage.
    for (Actor actor : stage.getActors()) {
      actor.remove();
    }
  }

  @Override
  public void render(float delta) {
    super.render(delta);

    // draw the main menu and the titel
    stage.getBatch().begin();
    game.skin.getFont("default").draw(stage.getBatch(), layout, menuX, menuY);
    stage.getBatch().end();
  }

  /**
   * creates the menu
   */
  private void createMenu() {
    // create a table for the table
    Table menu = new Table();
    float width = 140;
    float height = 25;
    float padding = 5;
    // set defaults
    menu.defaults().width(width);
    menu.defaults().height(height);
    menu.defaults().pad(padding);

    // creates the Pong button
    Button pongButton = new Button(game.skin);
    pongButton.add("Pong");
    // adds the listener to load Pong
    pongButton.addListener(new InputListener() {
      public boolean touchDown(InputEvent event, float x, float y, int pointer,
          int button) {
        // load Pong
        game.setScreen(new Pong(game));
        // dispose current screen
        dispose();
        return false;
      }
    });

    // creates the Snake button
    Button snakeButton = new Button(game.skin);
    snakeButton.add("Snake");
    // adds the listener to load Snake
    snakeButton.addListener(new InputListener() {
      public boolean touchDown(InputEvent event, float x, float y, int pointer,
          int button) {
        // load Snake
        game.setScreen(new Snake(game));
        // dispose current screen
        dispose();
        return false;
      }
    });

    // creates the Space Invader button
    Button spaceButton = new Button(game.skin);
    spaceButton.add("Space Invader");
    // adds listener to load Space Invader
    spaceButton.addListener(new InputListener() {
      public boolean touchDown(InputEvent event, float x, float y, int pointer,
          int button) {
        // load Space Invader
        game.setScreen(new SpaceInvader(game));
        // dispose current screen
        dispose();
        return false;
      }
    });

    // creates the Fabians Idea
    Button vr4m = new Button(game.skin);
    vr4m.add("Vr4m Jumps for you");
    vr4m.addListener(new InputListener() {
      public boolean touchDown(InputEvent event, float x, float y, int pointer,
          int button) {
        // load Fabian
        game.setScreen(new vr4m(game));
        // dispose current screen
        dispose();
        return false;
      }
    });

    // creates the Exit button
    Button swarm = new Button(game.skin);
    swarm.add("Swarm it");
    // adds listener to exit the game
    swarm.addListener(new InputListener() {
      public boolean touchDown(InputEvent event, float x, float y, int pointer,
          int button) {
        // no dispose is called, because it is done, by the exit method.
        game.setScreen(new Swarm(game));
        return false;
      }
    });

    // creates the Exit button
    Button exit = new Button(game.skin);
    exit.add("Exit");
    // adds listener to exit the game
    exit.addListener(new InputListener() {
      public boolean touchDown(InputEvent event, float x, float y, int pointer,
          int button) {
        // no dispose is called, because it is done, by the exit method.
        Gdx.app.exit();
        return false;
      }
    });

    // adds button to the table
    menu.add(pongButton);
    menu.row();
    menu.add(snakeButton);
    menu.row();
    menu.add(spaceButton);
    menu.row();
    menu.add(vr4m);
    menu.row();
    menu.add(swarm);
    menu.row();
    menu.add(exit);

    // position the table in the middle of the screen
    menu.setX(Oldies.width / 2);

    // calculates the position of the menu (cellHeight + padding) * numOfCells -
    // offsetFromTop
    menu.setY(Oldies.height - (height + padding) * menu.getCells().size - 30);

    // adds the table as actor to the stage
    stage.addActor(menu);
  }
}