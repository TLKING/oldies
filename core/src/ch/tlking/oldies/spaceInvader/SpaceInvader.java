package ch.tlking.oldies.spaceInvader;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

import ch.tlking.oldies.BasicScreen;
import ch.tlking.oldies.Oldies;

/**
 * @author Tobias
 */
public class SpaceInvader extends BasicScreen {
  private ShapeRenderer    shape;
  private Rectangle        player;
  private int              playerSpeed;
  private int              shotSpeed;
  private Array<Rectangle> shots;
  private Array<Rectangle> enemies;
  private boolean          enemyGoesRight;

  public SpaceInvader(Oldies game) {
    super(game);

    this.shape = new ShapeRenderer();
    this.shape.setColor(Color.WHITE);
    this.player = new Rectangle();
    this.player.width = 30;
    this.player.height = 15;
    this.player.x = (Oldies.width - player.width) / 2;
    this.player.y = 15;
    this.shots = new Array<Rectangle>();
    this.enemies = new Array<Rectangle>();

    this.resetEnemies();
    this.enemyGoesRight = true;

    this.playerSpeed = 400;
    this.shotSpeed = 500;
  }

  @Override
  public void render(float delta) {
    super.render(delta);

    shape.begin(ShapeType.Filled);
    shape.rect(player.x, player.y, player.width, player.height);
    for (Rectangle shot : shots) {
      shape.rect(shot.x, shot.y, shot.width, shot.height);
    }
    for (Rectangle enemy : enemies) {
      shape.rect(enemy.x, enemy.y, enemy.width, enemy.height);
    }
    shape.end();

    if (Gdx.input.isKeyPressed(Keys.D)) {
      player.x += playerSpeed * delta;

      if ((player.x + player.width) > Oldies.width) {
        player.x = Oldies.width - player.width;
      }
    }

    if (Gdx.input.isKeyPressed(Keys.A)) {
      player.x -= playerSpeed * delta;

      if ((player.x) < 0) {
        player.x = 0;
      }
    }

    if (Gdx.input.isKeyJustPressed(Keys.SPACE)) {
      Rectangle shot = new Rectangle();
      shot.height = 10;
      shot.width = 4;
      shot.y = player.y;
      shot.x = player.x + (player.width / 2);
      shots.add(shot);
    }

    Iterator<Rectangle> iter = shots.iterator();
    while (iter.hasNext()) {
      Rectangle shot = iter.next();
      shot.y += shotSpeed * delta;
      if (shot.y - shot.height < 0) {
        iter.remove();
      }
    }

    // TODO: IT IS FUCKING BROKEN!
    iter = enemies.iterator();
    while (iter.hasNext()) {
      Rectangle enemy = iter.next();
      if (enemyGoesRight) {
        enemy.x += (enemy.width + 20) * delta;
        if (enemy.x + enemy.width + 20 > Oldies.width) {
          enemyGoesRight = false;
        }
      } else {
        enemy.x -= (enemy.width + 20) * delta;
        if (enemy.x - enemy.width - 20 < 0) {
          enemyGoesRight = true;
        }
      }
    }
  }

  private void resetEnemies() {
    int enemySize = 20;
    int numOfColumns = (Oldies.width / (enemySize + 40)) * 1;
    int numOfRows = 3;
    for (int i = 0; i <= numOfRows; i++) {
      for (int j = 0; j <= numOfColumns; j++) {
        Rectangle enemy = new Rectangle();
        enemy.height = enemySize;
        enemy.width = enemySize;
        enemy.x = ((enemy.width + 20) * j) + 20;
        enemy.y = Oldies.height - 100 - ((enemy.height + 10) * i);
        enemies.add(enemy);
      }
    }
  }
}
