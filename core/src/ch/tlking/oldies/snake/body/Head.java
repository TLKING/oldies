/**
 * 
 */
package ch.tlking.oldies.snake.body;

import com.badlogic.gdx.graphics.Color;

import ch.tlking.oldies.Oldies;
import ch.tlking.oldies.snake.Snake.Direction;

public class Head extends Body {
  public Head(float width, float height, Color colour) {
    super(width, height, (Oldies.width - width) / 2,
        (Oldies.height - height) / 2, colour);
  }

  public void move(Direction direction) {
    if (snakeBody == null) {
      if (snakeBody == null) {
        snakeBody = new Body(hitBox.width, hitBox.height, hitBox.x, hitBox.y,
            shape.getColor());
      }
    }

    float tempX = hitBox.x;
    float tempY = hitBox.y;

    switch (direction) {
      case UP:
        hitBox.y += hitBox.height;
      break;
      case DOWN:
        hitBox.y -= hitBox.height;
      break;
      case RIGHT:
        hitBox.x += hitBox.width;
      break;
      case LEFT:
        hitBox.x -= hitBox.width;
    }

    snakeBody.move(tempX, tempY);
    if (add) {
      snakeBody.add();
      add = false;
    }
  }
}
