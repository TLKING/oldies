package ch.tlking.oldies.snake.body;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;

/**
 * @author Tobias
 */
public class Body {
  protected ShapeRenderer shape;
  protected Rectangle     hitBox;
  protected Body          snakeBody;
  protected boolean       add;

  public Body(float width, float height, float x, float y, Color colour) {
    this.hitBox = new Rectangle();
    this.hitBox.width = width;
    this.hitBox.height = height;

    this.hitBox.x = x;
    this.hitBox.y = y;

    this.add = false;
    this.snakeBody = null;

    this.shape = new ShapeRenderer();
    this.shape.setColor(colour);
  }

  public void move(float x, float y) {
    float tempX = hitBox.x;
    float tempY = hitBox.y;

    hitBox.x = x;
    hitBox.y = y;

    if (snakeBody != null) {
      snakeBody.move(tempX, tempY);
      if (this.add) {
        snakeBody.add();
        this.add = false;
      }
    } else {
      if (this.add) {
        snakeBody = new Body(hitBox.width, hitBox.height, hitBox.x, hitBox.y,
            shape.getColor());
        this.add = false;
      }
    }
  }

  public void add() {
    this.add = true;
  }

  public boolean onSnake(Rectangle head) {
    if (snakeBody == null) {
      return false;
    } else if (head.overlaps(snakeBody.hitBox)) {
      return true;
    } else {
      return snakeBody.onSnake(head);
    }
  }

  public void draw() {
    shape.begin(ShapeType.Filled);
    if (add) {
      shape.circle(hitBox.x + (hitBox.width / 2),
          hitBox.y + (hitBox.height / 2), hitBox.width / 2);
    } else {
      shape.rect(hitBox.x + 1, hitBox.y + 1, hitBox.width - 1,
          hitBox.height - 1);
    }
    shape.end();

    if (snakeBody != null) {
      snakeBody.draw();
    }
  }

  public void dispose() {
    shape.dispose();

    if (snakeBody != null) {
      snakeBody.dispose();
    }
  }

  public Rectangle getHitBox() {
    return this.hitBox;
  }
}
