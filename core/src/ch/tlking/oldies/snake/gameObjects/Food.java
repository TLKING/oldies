package ch.tlking.oldies.snake.gameObjects;

import java.util.concurrent.ThreadLocalRandom;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;

import ch.tlking.oldies.Oldies;
import ch.tlking.oldies.snake.body.Head;

/**
 * @author Tobias
 */
public class Food {
  private ShapeRenderer shape;
  private Rectangle     hitBox;
  private final int     points;

  public Food(float width, float height, Color colour, int points) {
    this.hitBox = new Rectangle();
    this.hitBox.width = width;
    this.hitBox.height = height;
    this.hitBox.x = -width;
    this.hitBox.y = -height;

    this.points = points;

    this.shape = new ShapeRenderer();
    this.shape.setColor(colour);
  }

  public void draw() {
    shape.begin(ShapeType.Filled);
    shape.rect(hitBox.x + 3, hitBox.y + 3, hitBox.width - 5, hitBox.height - 5);
    shape.end();
  }

  public void spawn(Head snake) {
    do {
      int randX = ThreadLocalRandom.current().nextInt(5,
          (Oldies.width - 40) / 5) * 5;
      int randY = ThreadLocalRandom.current().nextInt(5,
          (Oldies.height - 80) / 5) * 5;
      if ((randX % 10) == 0) {
        randX -= 5;
      }
      if ((randY % 10) == 0) {
        randY -= 5;
      }
      hitBox.x = randX;
      hitBox.y = randY;
    } while (snake.onSnake(hitBox));
  }

  public void delete() {
    hitBox.y *= -1;
  }

  public void dispose() {
    shape.dispose();
  }

  public boolean isFoodOnBoard() {
    return hitBox.y > 0;
  }

  public Rectangle getHitBox() {
    return this.hitBox;
  }

  public int getPoints() {
    return this.points;
  }
}
