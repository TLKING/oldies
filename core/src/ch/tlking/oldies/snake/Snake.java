/**
 * 
 */
package ch.tlking.oldies.snake;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.utils.TimeUtils;

import ch.tlking.oldies.BasicScreen;
import ch.tlking.oldies.Oldies;
import ch.tlking.oldies.snake.body.Head;
import ch.tlking.oldies.snake.gameObjects.Food;

/**
 * @author Tobias
 */
public class Snake extends BasicScreen {
  private GlyphLayout   scoreText;
  private GlyphLayout   gameOverText;
  private float         gameOverX;
  private float         gameOverY;
  private int           score;
  private ShapeRenderer border;
  private Rectangle     board;
  private Head          snake;
  private long          lastMove;
  private Direction     direction;
  private int           initBodySize;
  private Food          snak;
  private Food          goodie;
  private int           numOfSnaks;
  private boolean       stop;
  private Button        reset;

  public enum Direction {
    UP, DOWN, LEFT, RIGHT
  }

  /**
   * @param game
   */
  public Snake(final Oldies game) {
    super(game);

    this.border = new ShapeRenderer();
    this.border.setColor(Color.WHITE);
    this.board = new Rectangle();
    this.board.x = 14;
    this.board.y = 14;
    this.board.width = Oldies.width - 26;
    this.board.height = Oldies.height - 66;

    this.score = 0;
    this.scoreText = new GlyphLayout(game.skin.getFont("default"),
        "" + this.score);
    this.gameOverText = new GlyphLayout(game.skin.getFont("default"),
        "Game Over");

    this.reset();

    this.gameOverX = (Oldies.width - gameOverText.width) / 2;
    this.gameOverY = Oldies.height / 2;

    this.direction = Direction.LEFT;
    this.lastMove = TimeUtils.nanoTime();
    this.snak = new Food(10, 10, Color.WHITE, 10);
    this.goodie = new Food(10, 10, Color.RED, 15);

    this.snak.spawn(snake);
    this.numOfSnaks = 1;

    this.resetButton();
  }

  @Override
  public void render(float delta) {
    super.render(delta);

    border.begin(ShapeType.Line);
    border.rect(board.x, board.y, board.width, board.height);
    border.end();

    stage.getBatch().begin();
    game.skin.getFont("default").draw(stage.getBatch(), scoreText,
        Oldies.width - 35, Oldies.height - 20);

    if (stop) {
      game.skin.getFont("default").draw(stage.getBatch(), gameOverText,
          gameOverX, gameOverY);
    }
    stage.getBatch().end();

    snake.draw();
    goodie.draw();
    snak.draw();

    if (Gdx.input.isKeyPressed(Keys.W)) {
      if (direction != Direction.DOWN) {
        direction = Direction.UP;
      }
    }
    if (Gdx.input.isKeyPressed(Keys.D)) {
      if (direction != Direction.LEFT) {
        direction = Direction.RIGHT;
      }
    }
    if (Gdx.input.isKeyPressed(Keys.S)) {
      if (direction != Direction.UP) {
        direction = Direction.DOWN;
      }
    }
    if (Gdx.input.isKeyPressed(Keys.A)) {
      if (direction != Direction.RIGHT) {
        direction = Direction.LEFT;
      }
    }

    if (snake.onSnake(snake.getHitBox())
        || !board.contains(snake.getHitBox())) {
      stop = true;
    }

    if (numOfSnaks == 10) {
      goodie.spawn(snake);
      numOfSnaks = 0;
    }

    if (snake.getHitBox().overlaps(snak.getHitBox())) {
      score += snak.getPoints();
      scoreText.setText(game.skin.getFont("default"), "" + score);
      snak.spawn(snake);
      numOfSnaks++;
      snake.add();
    }

    if (snake.getHitBox().overlaps(goodie.getHitBox())) {
      score += goodie.getPoints();
      scoreText.setText(game.skin.getFont("default"), "" + score);
      goodie.delete();
      snake.add();
    }

    if (TimeUtils.nanoTime() - lastMove > (1e9 / 16) && !stop) {
      snake.move(direction);
      if (initBodySize > 0) {
        snake.add();
        initBodySize--;
      }
      lastMove = TimeUtils.nanoTime();
    }

  }

  public void dispose() {
    border.dispose();
    snake.dispose();
    snak.dispose();
  }

  private void reset() {
    if (snake != null) {
      snake.dispose();
    }
    initBodySize = 20; // body size - 1 cause head spawns one anyway
    score = 0;
    scoreText.setText(game.skin.getFont("default"), "" + score);
    snake = new Head(10, 10, Color.WHITE);
    stop = false;
  }

  private void resetButton() {
    reset = new Button(game.skin);
    reset.add("Reset");
    reset.setHeight(20);
    reset.setWidth(50);
    reset.setX(70);
    reset.setY(Oldies.height - 40);
    reset.addListener(new InputListener() {
      public boolean touchDown(InputEvent event, float x, float y, int pointer,
          int button) {
        reset();
        return false;
      }
    });

    stage.addActor(reset);
  }
}
