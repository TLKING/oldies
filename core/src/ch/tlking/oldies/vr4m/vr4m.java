package ch.tlking.oldies.vr4m;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

import ch.tlking.oldies.BasicScreen;
import ch.tlking.oldies.Oldies;

public class vr4m extends BasicScreen {
  private ShapeRenderer    shape;
  private Array<Rectangle> floors;
  private Rectangle        player;

  private int              playerSpeed;
  private int              gravity;
  private int              playerVerticalVelocity;
  private final int        jumpHeight;

  private boolean          isInAir;

  /**
   * @param game
   */
  public vr4m(Oldies game) {
    super(game);

    shape = new ShapeRenderer();
    this.shape.setColor(Color.WHITE);

    this.floors = new Array<Rectangle>();

    Rectangle floor = new Rectangle();
    floor.x = 0;
    floor.y = 0;
    floor.width = Oldies.width;
    floor.height = 20;

    floors.add(floor);
    for (int i = 0; i <= 5; i++) {
      Rectangle platform = new Rectangle();
      platform.x = 800 - (i * 150);
      platform.y = 30 + (i * 30);
      platform.width = 75;
      platform.height = 10;

      floors.add(platform);
    }

    this.player = new Rectangle();
    this.player.width = 8;
    this.player.height = 8;
    this.player.x = (Oldies.width - player.width) / 2 - 60;
    this.player.y = floor.height + 300;
    this.playerSpeed = 256;
    this.playerVerticalVelocity = 0;
    this.jumpHeight = 300;
    this.gravity = 512;
    this.isInAir = true;
  }

  @Override
  public void render(float delta) {
    super.render(delta);
    shape.begin(ShapeType.Filled);
    // draw floor
    for (Rectangle floor : floors) {
      shape.rect(floor.x, floor.y, floor.width, floor.height);
    }
    // draw player
    shape.rect(player.x, player.y, player.width, player.height);
    shape.end();

    if (Gdx.input.isKeyPressed(Keys.LEFT)) {
      player.x -= playerSpeed * delta;
      if ((player.x) < 0) {
        player.x = 0;
      }
    }

    if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
      player.x += playerSpeed * delta;
      if ((player.x + player.width) > Oldies.width) {
        player.x = Oldies.width - player.width;
      }
    }

    if (Gdx.input.isKeyJustPressed(Keys.UP) && !isInAir) {
      isInAir = true;
      playerVerticalVelocity = jumpHeight;
    }

    playerVerticalVelocity -= gravity * delta;

    for (Rectangle floor : floors) {
      if (player.overlaps(floor)) {
        player.y = floor.y + floor.height;
        isInAir = false;
        playerVerticalVelocity = 0;
      }
    }

    player.y += playerVerticalVelocity * delta;
  }

  @Override
  public void dispose() {
    shape.dispose();
  }
}